using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using Exceptions;
using Microsoft.Extensions.Configuration;
using Project = Types.Project;
using Action = Types.Action;



namespace PushAndTrigger;

class UserInterface
{
    private List<Project>? _projects;
    private readonly GitHandler _gitHandler = new();

    private UserInterface() { /* hidden */ }

    public static UserInterface Create()
    {
        var instance = new UserInterface();
        instance.Initialize();

        return instance;
    }

    public void ShowArgList()
    {
        if (_projects is null) throw new PreconditionException("_project not initialized");

        Console.WriteLine($"Your projects and Actions are:");
        _projects.ForEach(p => PrintProject(p));

        var first = _projects.First();
        Console.WriteLine($"\nYou could type for example: push-and-trigger '{first.Name}' '{first.Actions.First().Name}' 'currentBranch'\n");

    }

    public void HandleArgs(string maybeProject, string maybeAction, string activeBranch)
    {
        Project project;
        Action action;

        // first arg: project
        try
        {
            project = CheckAndGetProject(maybeProject);
        }
        catch (ArgumentException)
        {
            Console.WriteLine($"This project is not specified.\n");
            ShowArgList();
            return;
        }

        // second arg: action
        try
        {
            action = CheckAndGetAction(project, maybeAction);
        }
        catch (ArgumentException)
        {
            Console.WriteLine($"This action is not specified for this project.\n");
            ShowArgList();
            return;
        }

        // third arg: branch
        if (!_gitHandler.CheckBranch(activeBranch))
        {
            Console.WriteLine($"You are not on the specified branch\n");
            return;
        }

        ProceedWithValidatedArgs(project, action, activeBranch);
    }

    private void Initialize()
    {
        // build config
        IConfigurationRoot? configuration;
        try
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.SetBasePath(GetBasePath());
            configurationBuilder.AddJsonFile("appsettings.json", false);
            configuration = configurationBuilder.Build();
        }
        catch (Exception e)
        {
            Console.Error.WriteLine($"There is a problem with appsettings.json: {e.Message}");
            return;
        }

        // initialize projects from config
        var projects = new List<Project>();
        try
        {
            var projectSection = configuration.GetSection("Projects");

            projectSection.Bind(projects);
            if (projects is null || projects.Count == 0)
                throw new JsonException("Section 'Projects' is missing. Please refer to the Readme.");
            _projects = projects;
        }
        catch (Exception e)
        {
            Console.Error.WriteLine($"Parsing your projects went wrong: {e.Message}");
        }
    }

    private void PrintProject(Project p)
    {
        Console.WriteLine($"{p.Name}");
        p.Actions.ForEach(a => Console.WriteLine($" - {a.Name}"));
    }


    private Project CheckAndGetProject(string maybeProject)
    {
        if (_projects is null) throw new PreconditionException("_project not initialized");

        Project? found = _projects.Find(p => p.Name == maybeProject) ??
            throw new ArgumentException("Project not found");
        return found;
    }

    private static Action CheckAndGetAction(Project project, string maybeAction)
    {
        Action? found = project.Actions.Find(a => a.Name == maybeAction) ??
            throw new ArgumentException("Action not found");
        return found;
    }

    private void ProceedWithValidatedArgs(Project p, Action a, string activeBranch)
    {
        _gitHandler.PushAndTrigger(p, a, activeBranch);
    }

    private string GetBasePath()
    {
        using var processModule = Process.GetCurrentProcess().MainModule;
        return Path.GetDirectoryName(processModule?.FileName) ?? throw new Exception("GetBasePath went wrong");
    }
}