
using System;
using System.IO;
using System.Management.Automation;
using Exceptions;
using PwShell;

namespace PushAndTrigger;


class GitHandler
{
    private readonly PowerShellHandler _shell = new PowerShellHandler();

    public void PushAndTrigger(Types.Project project, Types.Action action, string activeBranch)
    {
        var snippets = new PwsSnippets
        (
            project.URLofOrigin,
            activeBranch,
            project.AbsoluteHookDirectory,
            action.Branch
        );

        if (ExecutesWithError(snippets.IsGitRepo)) return;
        if (ExecutesWithError(snippets.ValidateOriginOrThrow)) return;
        if (ExecutesWithError(snippets.ValidateBranchOrThrow)) return;
        if (ExecutesWithError(snippets.GitPush)) return;
        Console.WriteLine($"git push");
        if (ExecutesWithError(snippets.ChangeToHookDirectory)) return;
        //Console.WriteLine($"change to hook finished");
        if (ExecutesWithError(snippets.ChangeToActionBranch)) return;
        //Console.WriteLine($"switch branch finished");
        if (ExecutesWithError(snippets.WriteToLogFile)) return;
        //Console.WriteLine($"write to logfile finished");
        if (ExecutesWithError(snippets.GitAdd)) return;
        //Console.WriteLine($"git add finished");
        if (ExecutesWithError(snippets.GitCommit)) return;
        //Console.WriteLine($"git commit finished");
        if (ExecutesWithError(snippets.GitPush)) return;
        Console.WriteLine($"trigger hook");
        Console.WriteLine($"done!");
    }

    public bool CheckBranch(string branch)
    {
        return !ExecutesWithError(PwsSnippets.GetValidateBranchOrThrow(branch));
    }

    private bool ExecutesWithError(string shellCode)
    {
        try
        {
            _shell.Execute(shellCode);
        }
        catch( PowerShellException)
        {
            // Message was already printed in Powershellhandler
            return true;
        }
        return false;
    }
}