# PushAndTrigger

### Idea

This commandline app allows us to push to a repo and trigger some gitlab / github action associated **with another
branch**. That way, we can for example have our actions to deploy to a custom server in a private repo while 
developing a public project. Especially if we want to use custom runners (e.g. the server that pulls the repo and 
runs the app) keeping the actions private is recommended in the docs.

### Setting up the actions repo

In the group https://gitlab.com/howto-automated_deployment we have an example project (custom_actions_example). In 
order to use PushAndTrigger we need to set up such a (private) repo for each project we want to use the app for. Each
action that we want to provide lives in its own branch.

Make sure, all the names and urls are set correctly in appsettins.json!


### Installation

1)
    Clone this repo and run
    
        // the powershell module is not available for win-x64
        dotnet publish -c release -f net7.0 -r win7-x86 -o C:\MyPublish\PushAndTrigger
    
    Or pull the already published .exe:
    
        git clone https://gitlab.com/howto-automated_deployment/publish_v1.0_win7-x86
    
2) Set up your actions as described above.
    
3) update appsettings.json accordingly.

4) Add the location of the .exe to the system environment variable PATH.

5) Reload your shell.

6) Type `pull-and-trigger <projectname> <actionname> <branch>` 

